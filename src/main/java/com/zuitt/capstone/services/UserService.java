package com.zuitt.capstone.services;

import com.zuitt.capstone.models.User;
import org.springframework.http.ResponseEntity;

import java.util.Optional;

public interface UserService {

    //    Optional - defines if the method may/may not return an object of the User Class
    Optional<User> findByUsername(String username);

    //    Create
    void createUser(User user);

    //    View all
    Iterable<User> getUsers();

    //    Delete
    ResponseEntity deleteUser(Long id);

    //    Update
    ResponseEntity updateUser(Long id, User user);
}