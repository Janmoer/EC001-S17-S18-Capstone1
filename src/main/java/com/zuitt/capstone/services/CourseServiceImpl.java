package com.zuitt.capstone.services;

import com.zuitt.capstone.config.JwtToken;
import com.zuitt.capstone.models.Course;
import com.zuitt.capstone.models.User;
import com.zuitt.capstone.repositories.CourseRepository;
import com.zuitt.capstone.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class CourseServiceImpl implements CourseService{

    //    An object cannot be instantiated from interfaces
//    @Autowired allow us to use the interface as if it was an instance of an
//    object and allows us to use the methods from the CRUDRepository
    @Autowired
    private CourseRepository courseRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    JwtToken jwtToken;

    //    Create Course
    public void createCourse(String stringToken, Course course){
        User author = userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));

        Course newCourse = new Course();
        newCourse.setName(course.getName());
        newCourse.setDescription(course.getDescription());
        newCourse.setPrice(course.getPrice());
        newCourse.setUser(author);
        courseRepository.save(newCourse);
    }

    //    Get All Course
    public Iterable<Course> getCourses(){
        return courseRepository.findAll();
    }

    //  Delete Course
    public ResponseEntity deleteCourse(Long id, String stringToken){
        Course courseForDeleting = courseRepository.findById(id).get();
        String courseAuthorName = courseForDeleting.getUser().getUsername();
        String authenticatedUsername = jwtToken.getUsernameFromToken(stringToken);

        if (authenticatedUsername.equals(courseAuthorName)) {
            courseRepository.deleteById(id);
            return new ResponseEntity<>("Course Deleted Successfully.", HttpStatus.OK);
        } else {
            return new ResponseEntity<>("You are not authorized to delete this course.", HttpStatus.UNAUTHORIZED);
        }

    }

    //  Update a Course
    public ResponseEntity updateCourse(Long id, String stringToken, Course course) {
        Course courseForUpdating = courseRepository.findById(id).get();
//        Get the "author" of the specific post.
        String courseAuthorName = courseForUpdating.getUser().getUsername();
//        Get the "username" from the stringToken to compare it with the username of the current post being edited.
        String authenticatedUserName = jwtToken.getUsernameFromToken(stringToken);

//        Check if the username of the authenticated user matches the username of the post's author
        if (authenticatedUserName.equals(courseAuthorName)) {
            courseForUpdating.setName(course.getName());
            courseForUpdating.setDescription(course.getDescription());
            courseForUpdating.setPrice(course.getPrice());

            courseRepository.save(courseForUpdating);

            return new ResponseEntity<>("Course updated successfully", HttpStatus.OK);
        } else {
            return new ResponseEntity<>("You are not authorized to edit this course.", HttpStatus.UNAUTHORIZED);
        }
    }
        public Iterable<Course> getMyCourses (String stringToken){
            User author = userRepository.findByUsername((jwtToken.getUsernameFromToken(stringToken)));
            return author.getCourses();
        }
}
